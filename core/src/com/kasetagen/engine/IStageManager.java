package com.kasetagen.engine;

import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 2:07 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IStageManager {

    Stage getStage();
}
