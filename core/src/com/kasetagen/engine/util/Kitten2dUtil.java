package com.kasetagen.engine.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenActor;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenGroup;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 2/9/15
 * Time: 9:39 PM
 */
public class Kitten2dUtil {

    public static void cleanUpActors(Array<Actor> actors) {
        //Remove any removable actors from the stage prior to performing the
        //  normal act.
        for(Actor a:actors){
            if((a instanceof KasetagenActor && ((KasetagenActor)a).isRemovable()) ||
               (a instanceof KasetagenGroup && ((KasetagenGroup)a).isRemovable())){
                a.remove();
            }
        }
    }
}
