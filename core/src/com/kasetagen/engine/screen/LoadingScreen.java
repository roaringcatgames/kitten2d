package com.kasetagen.engine.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.AnimatedActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.Progress;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 2:07 AM
 */
public class LoadingScreen extends Kitten2dScreen {

    TextureAtlas loadingAtlas;
    Animation loadingAnimation;
    AnimatedActor loadingImage;
    boolean isAnimatedWithProgress = false;
    Progress progress;

    public LoadingScreen(IGameProcessor delegate, String atlasName, String animationName, Stage stage) {
        super(delegate);
        init(delegate, atlasName, animationName, stage);
    }
    public LoadingScreen(IGameProcessor delegate, String atlasName, Stage stage) {
        super(delegate);
        init(delegate, atlasName, "Loading", stage);
    }

    private void init(IGameProcessor delegate, String atlasName, String animationName, Stage stage){
        this.stage = stage != null ? stage : new Stage();

        loadingAtlas = new TextureAtlas(Gdx.files.internal(atlasName));
        loadingAnimation = new Animation(1f/3f, loadingAtlas.findRegions(animationName));

        loadingImage = new AnimatedActor(0, 0, 1280f, 720f, loadingAnimation, 0f);
        loadingImage.setIsLooping(false);
        stage.addActor(loadingImage);
        progress = new Progress(0f, 0f, 1280f, 20f, null, Color.CYAN);
        stage.addActor(progress);
    }
    public void setIsAnimatedWithProgress(boolean shouldAnimateWithProgress){
        isAnimatedWithProgress = shouldAnimateWithProgress;
    }


    @Override
    public void render(float delta) {
        if(gameProcessor.isLoaded()){
            gameProcessor.changeToScreen(gameProcessor.getStartScreen());
        }else{
            int numberOfFrames = loadingAnimation.getKeyFrames().length;
            float loadingProgress = gameProcessor.getLoadingProgress();

            if(isAnimatedWithProgress){
                int targetFrame = Math.round(loadingProgress * numberOfFrames);
                loadingImage.setTargetKeyFrame(targetFrame);
            }
            progress.setProgress(loadingProgress);
        }

        super.render(delta);
    }
}
