package com.kasetagen.engine.gdx.scenes.scene2d;

import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/25/15
 * Time: 1:37 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IActorSpawner<T extends GenericActor> {

    public T spawn();
}
