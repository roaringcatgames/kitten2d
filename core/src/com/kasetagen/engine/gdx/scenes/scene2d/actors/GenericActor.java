package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/26/14
 * Time: 3:04 PM
 */
public class GenericActor extends KasetagenActor {

    protected TextureRegion textureRegion;
    public Rectangle collider;

    public Vector2 velocity = new Vector2(0f, 0f);

    protected float rotationSpeed = 0f;

    private Array<ActorDecorator> decorations;

    public GenericActor(float x, float y, float width, float height, TextureRegion textureRegion, Color color){
        super();
        setPosition(x, y);
        setBounds(x, y, width, height);
        setWidth(width);
        setHeight(height);
        setOrigin(width/2, height/2);
        if(color == null){
            color = Color.BLACK;
        }
        setColor(color);
        if(textureRegion != null){
            this.textureRegion = textureRegion;
        }
        collider = new Rectangle(x, y, width, height);
        decorations = new Array<ActorDecorator>();
    }

    protected void adjustCollidingBox(float delta){

        if(hasParent()){
            float x = getParent().getX() + getX();
            float y = getParent().getY() + getY();
            collider.setPosition(x, y);
        }else{
            collider.setPosition(getX(), getY());
        }
        collider.setWidth(getWidth());
        collider.setHeight(getHeight());
    }

    protected void adjustRotation(float delta){
        setRotation(getRotation() + (rotationSpeed * delta));
    }

    public float getRotationSpeed(){
        return rotationSpeed;
    }

    public void setRotationSpeed(float newSpeed){
        rotationSpeed = newSpeed;
    }

    protected void adjustPosition(float delta){
        float curX = getX();
        float curY = getY();
        float newX = curX + (velocity.x * delta);
        float newY = curY + (velocity.y * delta);
        setPosition(newX, newY);

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        adjustRotation(delta);
        adjustPosition(delta);
        adjustCollidingBox(delta);

        for(ActorDecorator d:decorations){
            d.applyAdjustment(this, delta);
        }
    }

    @Override
    protected void drawFull(Batch batch, float parentAlpha) {

        if(textureRegion != null){
            batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                    getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
    }

    public void addDecorator(ActorDecorator d){
        this.decorations.add(d);
    }

    public void removeDecorator(ActorDecorator d){
        this.decorations.removeValue(d, true);
    }

    public void flipTextureRegion(boolean flipX, boolean flipY){
        if(textureRegion != null){
            textureRegion.flip(flipX, flipY);
        }
    }
}