package com.kasetagen.engine.gdx.scenes.scene2d.decorators;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.util.MathUtil;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 4/9/15
 * Time: 5:47 PM
 */
public class PulsingScaleDecorator implements ActorDecorator {

    private float scaleFactor = 0f;
    private float secondsBetweenMinAndMax = 1f;
    private float elapsedTime = 0f;

    public PulsingScaleDecorator(float scaleAdjust, float secondsBetweenMinMax){
        this.scaleFactor = scaleAdjust;
        this.secondsBetweenMinAndMax = secondsBetweenMinMax;

    }

    @Override
    public void applyAdjustment(Actor a, float delta) {
        elapsedTime += delta;
        float scaleAdjust = (float) MathUtil.getSineYForTime(secondsBetweenMinAndMax, scaleFactor, elapsedTime);
        a.setScale(1f+scaleAdjust);
    }
}
