package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.kasetagen.engine.util.Kitten2dUtil;

public abstract class KasetagenGroup extends Group {

	protected static ShapeRenderer debugRenderer = new ShapeRenderer();

    private Array<ActorDecorator> decorations;
    private IActorDisposer disposer;
    private boolean isRemovable = false;

    public KasetagenGroup(){
        decorations = new Array<ActorDecorator>();
    }

    @Override
    public void act(float delta) {
        cleanUpActors();
        super.act(delta);

        for(ActorDecorator d:decorations){
            d.applyAdjustment(this, delta);
        }
    }

    private void cleanUpActors() {
        Kitten2dUtil.cleanUpActors(getChildren());
    }

    @Override
    public boolean remove() {
        //We have to have this because the scene2d Stage.addActor() method initially calls
        //  actor.remove() before adding it to the stage. This will break if we start sharing
        //  actors between stages :/
        if(this.hasParent() && disposer != null){
            disposer.dispose(this);
        }
        return super.remove();    //To change body of overridden methods use File | Settings | File Templates.
    }

	public void draw(Batch batch, float parentAlpha) {
        Color bc = batch.getColor();
        batch.setColor(bc.r, bc.g, bc.b, bc.a*parentAlpha);
        drawBefore(batch, parentAlpha);
		super.draw(batch, parentAlpha);
		drawFull(batch, parentAlpha);
		drawAfter(batch, parentAlpha);
	}

    protected  void drawBefore(Batch batch, float parentAlpha){
        //Does nothing by default
    }
	protected abstract void drawFull(Batch batch, float parentAlpha);
	
	protected void drawAfter(Batch batch, float parentAlpha){
        KasetagenScene2dUtil.debugRenderScene2dNode(batch, parentAlpha, debugRenderer, this);
	}



    public void addDecorator(ActorDecorator d){
        this.decorations.add(d);
    }

    public void removeDecorator(ActorDecorator d){
        this.decorations.removeValue(d, true);
    }

    public void setIsRemovable(boolean isRemovable){
        this.isRemovable = isRemovable;
    }

    public boolean isRemovable(){
        return this.isRemovable;
    }

    public void setDisposer(IActorDisposer disposer){
        this.disposer = disposer;
    }
}
