package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.Cinematic;
import com.kasetagen.engine.util.Kitten2dUtil;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 11/16/14
 * Time: 12:23 PM
 */
public class Kitten2dStage extends Stage {

    Array<ICameraModifier> cameraMods;
    protected IGameProcessor gameProcessor;
    protected Cinematic cinematic;
    protected boolean cinematicComplete = false;

    public Kitten2dStage(IGameProcessor gp) {
        super();
        this.gameProcessor = gp;
        cameraMods = new Array<ICameraModifier>();
    }

    public Kitten2dStage(Viewport vp, IGameProcessor gp){
        super(vp);
        this.gameProcessor = gp;
        cameraMods = new Array<ICameraModifier>();
    }

    @Override
    public void act(float delta) {
        if(!cinematicComplete){
            if(cinematic != null && cinematic.isComplete()){
                onCinematicComplete();
            }
        }
        cleanUpActors();
        super.act(delta);
        //Process Camera Mods After the act
        processCameraMods(delta);
    }

    @Override
    public void act() {
        cleanUpActors();
        super.act();
    }

    private void processCameraMods(float delta) {
        Iterator<ICameraModifier> itr = cameraMods.iterator();
        while(itr.hasNext()){
            ICameraModifier mod = itr.next();
            mod.modify(getCamera(), delta);
            if(mod.isComplete()){
                itr.remove();
            }
        }
    }

    private void cleanUpActors() {
        Kitten2dUtil.cleanUpActors(getActors());
    }

    public void addCameraMod(ICameraModifier mod){
        cameraMods.add(mod);
    }

    public void onCinematicComplete(){
        cinematicComplete = true;
    }
}
