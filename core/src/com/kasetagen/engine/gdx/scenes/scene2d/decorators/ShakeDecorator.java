package com.kasetagen.engine.gdx.scenes.scene2d.decorators;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.util.MathUtil;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 10:32 AM
 */
public class ShakeDecorator implements ActorDecorator{

    private float xScale;
    private float yScale;
    private float xShakeSpeed;
    private float yShakeSpeed;
    private float elapsedTime;

    private float lastXSinePos;
    private float lastYSinePos;

    /**
     * Applies an x and y offset to an actor at shake intervals based on
     * given scales. Uses a simple Sine function to determine the offset for the
     * time point.
     * @param xScale - Total range for shaking on x axis(equal adjustment on left and right)
     * @param yScale - Total range for shaking on y axis (equal adjustment on top and bottom)
     * @param shakeSpeed - Seconds between -Scale and +Scale ranges.
     */
    public ShakeDecorator(float xScale, float yScale, float shakeSpeed){
        init(xScale, yScale, shakeSpeed, shakeSpeed);
    }

    public ShakeDecorator(float xScale, float yScale, float xShakeSpeed, float yShakeSpeed){
        init(xScale, yScale, xShakeSpeed, yShakeSpeed);
    }


    private void init(float xScale, float yScale, float xShakeSpeed, float yShakeSpeed){
        this.xScale = xScale;
        this.yScale = yScale;
        this.xShakeSpeed = xShakeSpeed;
        this.yShakeSpeed = yShakeSpeed;

        this.elapsedTime = 0f;
    }

    @Override
    public void applyAdjustment(Actor a, float delta) {
        elapsedTime += delta;


        float currentXSinePos = (float)MathUtil.getSineYForTime(xShakeSpeed, xScale, elapsedTime);
        float targetX = a.getX() + currentXSinePos - lastXSinePos;
        lastXSinePos = currentXSinePos;
        float currentYSinePos = (float)MathUtil.getSineYForTime(yShakeSpeed, yScale, elapsedTime);
        float targetY = a.getY() + currentYSinePos - lastYSinePos;
        lastYSinePos = currentYSinePos;
        a.setPosition(targetX, targetY);
    }

    public void setShakeSpeed(float speed){
        xShakeSpeed = speed;
        yShakeSpeed = speed;
    }

    public void setShakeSpeed(float x, float y){
        xShakeSpeed = x;
        yShakeSpeed = y;
    }
}
