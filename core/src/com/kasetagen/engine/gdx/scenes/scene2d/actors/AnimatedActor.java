package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/26/14
 * Time: 3:19 PM
 */
public class AnimatedActor extends GenericActor{

    public static final String DEFAULT_STATE = "DEFAULT";
    public Animation animation;

    private float keyFrameTime = 0f;

    private int targetKeyFrame = -1;

    protected ObjectMap<String, Animation> stateAnimations;

    protected boolean isLooping = true;
    protected String currentState = DEFAULT_STATE;
    protected boolean isPaused = false;


    public AnimatedActor(float x, float y, float width, float height, Animation animation, float startKeyframe){
        super(x, y, width, height, null, Color.GREEN);
        keyFrameTime = startKeyframe;
        this.animation = animation;
        this.stateAnimations = new ObjectMap<String, Animation>();
        if(animation != null){
            stateAnimations.put(DEFAULT_STATE, animation);
        }
    }

    public void setTargetKeyFrame(int keyFrame){
        if(animation != null && keyFrame < animation.getKeyFrames().length ){
            targetKeyFrame = keyFrame;
        }
    }

    public void addStateAnimation(String state, Animation ani){
        stateAnimations.put(state, ani);
    }

    public void setState(String state, boolean shouldResetTimeFrame){
        if(stateAnimations.containsKey(state)){
            animation = stateAnimations.get(state);
            if(shouldResetTimeFrame){
                keyFrameTime = 0f;
            }
            currentState = state;
        }
    }

    public String getCurrentState(){
        return currentState;
    }

    public void setState(String state){
        setState(state, false);
    }

    public void setIsLooping(boolean shouldLoop){
        isLooping = shouldLoop;
    }

    public void restart(){
        keyFrameTime = 0f;
        if(isPaused){
            resume();
        }
    }

    public void pause(){
        this.isPaused = true;
        if(animation != null){
            textureRegion = animation.getKeyFrame(keyFrameTime, isLooping);
        }
    }

    public void resume(){
        this.isPaused = false;
    }

    public boolean isAnimationComplete(){
        if(animation!= null){
            return !isLooping && animation.isAnimationFinished(keyFrameTime);
        }else{
            return true;
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(animation != null && !isPaused){
            if(targetKeyFrame < 0){
                textureRegion = animation.getKeyFrame(keyFrameTime, isLooping);
                keyFrameTime += delta;
            }else{
                textureRegion = animation.getKeyFrames()[targetKeyFrame];
            }
        }
    }

    @Override
    public void flipTextureRegion(boolean flipX, boolean flipY) {
        for(Animation ani:stateAnimations.values()){
            for(TextureRegion tr:ani.getKeyFrames()){
                tr.flip(flipX, flipY);
            }
        }
    }
}

