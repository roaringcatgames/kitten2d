package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kasetagen.engine.IGameProcessor;
import com.kasetagen.engine.screen.Kitten2dScreen;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 9:09 AM
 */
public abstract class StagedKitten2dScreen extends Kitten2dScreen {

    public StagedKitten2dScreen(IGameProcessor gp) {
        super(gp);
        this.stage = createStage();
        initialize();
    }

    protected abstract Stage createStage();

    protected abstract void initialize();
}
