package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 11/19/14
 * Time: 7:26 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IActorDisposer{
    public void dispose(Actor a);
}
