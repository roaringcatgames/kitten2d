package com.kasetagen.engine.gdx.scenes.scene2d;

public class KasetagenStateUtil {

	private static boolean isDebugMode = false;
	public static void setDebugMode(boolean isInDebugMode){
		isDebugMode = isInDebugMode;
	}
	
	public static boolean isDebugMode(){
		return isDebugMode;
	}
}
