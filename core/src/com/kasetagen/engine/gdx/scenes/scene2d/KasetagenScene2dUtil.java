package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 6/2/14
 * Time: 9:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class KasetagenScene2dUtil {

    public static void debugRenderScene2dNode(Batch batch, float parentAlpha, ShapeRenderer debugRenderer, Actor a){
        if(batch != null &&
           debugRenderer != null &&
           a != null &&
           KasetagenStateUtil.isDebugMode()){
                //Flush the batch, rendering it's contents to the screen then re-open for our debug lines
                batch.end();
                batch.begin();
                Gdx.gl20.glLineWidth(1f);
                //Set the projection matrix, and line shape
                debugRenderer.setProjectionMatrix(a.getStage().getCamera().combined);
                debugRenderer.begin(ShapeRenderer.ShapeType.Line);
                //Draw the red origin marker
                debugRenderer.setColor(Color.RED);
                if(a.hasParent()){
                    debugRenderer.circle(a.getParent().getX() + a.getX() + a.getOriginX(),
                                         a.getParent().getY() + a.getY() + a.getOriginY(), 3f);
                }else{
                    debugRenderer.circle(a.getX() + a.getOriginX(), a.getY() + a.getOriginY(), 3f);
                }
                //Draw the bounds of the actor as a box
                Color c = a.getColor() != null ? a.getColor() : Color.WHITE;
                debugRenderer.setColor(c);
                if(a.hasParent()){
                    debugRenderer.rect(a.getParent().getX() + a.getX(),
                                       a.getParent().getY() + a.getY(),
                                       a.getWidth(), a.getHeight());
                }else{
                    debugRenderer.rect(a.getX(), a.getY(), a.getWidth(), a.getHeight());
                }

                if(a instanceof GenericActor){
                    debugRenderer.setColor(Color.PINK);
                    Rectangle rect = ((GenericActor)a).collider;
                    debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
                }

                if(a instanceof GenericGroup){
                    debugRenderer.setColor(Color.MAGENTA);
                    Rectangle rect = ((GenericGroup)a).collider;
                    debugRenderer.rect(rect.x, rect.y, rect.width, rect.height);
                }

                //End our shapeRenderer, flush the batch, and re-open it for future use as it was open
                // coming in.
                debugRenderer.end();
                batch.end();
                batch.begin();


        }
    }
}
