package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public abstract class KasetagenActor extends Actor {

	protected static  ShapeRenderer debugRenderer = new ShapeRenderer();

    private Array<ActorDecorator> decorations;
    private boolean isRemovable = false;

    private IActorDisposer disposer;

    public KasetagenActor(){
        decorations = new Array<ActorDecorator>();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        for(ActorDecorator d:decorations){
            d.applyAdjustment(this, delta);
        }
    }

    @Override
    public boolean remove() {
        //We have to have this because the scene2d Stage.addActor() method initially calls
        //  actor.remove() before adding it to the stage. This will break if we start sharing
        //  actors between stages :/
        if(this.hasParent() && disposer != null){
            disposer.dispose(this);
        }
        return super.remove();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void draw(Batch batch, float parentAlpha) {
        Color bc = batch.getColor();
        batch.setColor(bc.r, bc.g, bc.b, bc.a*parentAlpha);
        drawBefore(batch, parentAlpha);
		super.draw(batch, parentAlpha);
		drawFull(batch, parentAlpha);
		drawAfter(batch, parentAlpha);
	}

    /**
     * This Method performs no action by default, but can be used to
     * render things behind the default Actor rendering.
     *
     * @param batch
     * @param parentAlpha
     */
    protected  void drawBefore(Batch batch, float parentAlpha){
    }

	protected abstract void drawFull(Batch batch, float parentAlpha);
	
	protected void drawAfter(Batch batch, float parentAlpha){
        KasetagenScene2dUtil.debugRenderScene2dNode(batch, parentAlpha, debugRenderer, this);
	}

    public void addDecorator(ActorDecorator d){
        this.decorations.add(d);
    }

    public void removeDecorator(ActorDecorator d){
        this.decorations.removeValue(d, true);
    }

    public void setIsRemovable(boolean isRemovable){
        this.isRemovable = isRemovable;
    }

    public boolean isRemovable(){
        return this.isRemovable;
    }

    public void setDisposer(IActorDisposer disposer){
        this.disposer = disposer;
    }
}
