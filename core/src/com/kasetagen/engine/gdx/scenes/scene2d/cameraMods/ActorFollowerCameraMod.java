package com.kasetagen.engine.gdx.scenes.scene2d.cameraMods;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ICameraModifier;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 5:28 PM
 */
public class ActorFollowerCameraMod implements ICameraModifier {

    private boolean isEnabled = false;

    private Actor actorToFollow;
    private float triggerRange;
    private float leftBounds;
    private float rightBounds;

    public ActorFollowerCameraMod(Actor actorToFollow, float triggerRange, float leftBounds, float rightBounds){
        this.actorToFollow = actorToFollow;
        this.triggerRange = triggerRange;
        this.leftBounds = leftBounds;
        this.rightBounds = rightBounds;
    }

    public void setEnabled(boolean enabled){
        isEnabled = enabled;
    }

    @Override
    public void modify(Camera camera, float v) {
        if(isEnabled){
            /**
             * UPDATE CAMERA POSITION WITHIN BOUNDS
             */
            float cameraX = camera.position.x;
            float targetCamerPosition = cameraX;
            if(actorToFollow.getRight() > cameraX + triggerRange){
                targetCamerPosition += actorToFollow.getRight() - (cameraX + triggerRange);
            }else if(actorToFollow.getX() < cameraX - triggerRange){
                targetCamerPosition -= (cameraX - triggerRange) - actorToFollow.getX();
            }

            if(targetCamerPosition < leftBounds){
                targetCamerPosition = leftBounds;
            }else if(targetCamerPosition > rightBounds){
                targetCamerPosition = rightBounds;
            }

            if(targetCamerPosition != cameraX){
                camera.position.x = targetCamerPosition;
            }
        }
    }

    @Override
    public boolean isComplete() {
        return false;
    }
}
