package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.kasetagen.engine.gdx.scenes.scene2d.KasetagenGroup;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/26/14
 * Time: 3:10 PM
 * To change this template use File | Settings | File Templates.
 */

public class GenericGroup extends KasetagenGroup {
    protected TextureRegion textureRegion;
    public Rectangle collider;
    public Vector2 velocity = new Vector2(0f, 0f);
    protected float rotationSpeed = 0f;
    /**
     * Create a new GenericGroup actor at x, y, with dimensions width, height. IF a texture region is provided
     * it will be drawn in the background of any actors added to the group.
     * @param x - stage x coordinate position
     * @param y - stage y coordinate position
     * @param width - width of group
     * @param height - height of group
     * @param textureRegion - Optional (can be null). Drawn to the size of the group behind any actors added to the group.
     * @param color - Color used for debugging.
     */
    public GenericGroup(float x, float y, float width, float height, TextureRegion textureRegion, Color color){
        super();
        setPosition(x, y);
        setBounds(x, y, width, height);
        setWidth(width);
        setHeight(height);
        setOrigin(width/2, height/2);
        setColor(color);
        if(textureRegion != null){
            this.textureRegion = textureRegion;
        }
        collider = new Rectangle(x, y, width, height);
    }

    protected void adjustRotation(float delta){
        setRotation(getRotation() + (rotationSpeed * delta));
    }

    public float getRotationSpeed(){
        return rotationSpeed;
    }

    public void setRotationSpeed(float newSpeed){
        rotationSpeed = newSpeed;
    }

    protected void adjustCollidingBox(float delta){
        collider.setPosition(getX(), getY());
        collider.setWidth(getWidth());
        collider.setHeight(getHeight());
    }

    protected void adjustPosition(float delta){
        float curX = getX();
        float curY = getY();
        float newX = curX + (velocity.x * delta);
        float newY = curY + (velocity.y * delta);
        setPosition(newX, newY);

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        adjustRotation(delta);
        adjustPosition(delta);
        adjustCollidingBox(delta);

    }

    @Override
    protected void drawBefore(Batch batch, float parentAlpha) {
        if(textureRegion != null){
            batch.draw(textureRegion, getX(), getY(), getOriginX(), getOriginY(),
                    getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
    }

    @Override
    protected void drawFull(Batch batch, float v) {

    }
}

