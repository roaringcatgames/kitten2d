package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/30/14
 * Time: 9:02 PM
 */
public class Cinematic extends GenericGroup {

    private Animation animation;
    private boolean shouldLoop;
    private Array<CinematicScene> scenes;
    private boolean isComplete = false;
    private int index = 0;
    private boolean isStarted = false;
    private Vector2 initialPosition;
    private Color clearColor;

    private boolean requiresContinue = false;

    public Cinematic(float x, float y, float width, float height, Animation animation, boolean shouldLoop, Color clearColor){
        super(x, y, width, height, null, Color.BLACK);
        this.clearColor = clearColor;
        this.initialPosition = new Vector2(x, y);
        this.animation = animation;
        this.shouldLoop = shouldLoop;
        this.scenes = new Array<CinematicScene>();
    }

    public void addScene(CinematicScene scene){
        if(animation != null && animation.getKeyFrames().length > scenes.size){
            scenes.add(scene);
            scene.index = scenes.size -1;
            //Gdx.app.log("CINEMATIC", "Scene Added. Animation Frames: " + animation.getKeyFrames().length + " Scene Count: " + scenes.size);
        }else{
            Gdx.app.log("CINEMATIC", "Added Scene is out of bounds of Animation Frames");
        }
    }

    public void setRequiresContinue(boolean requiresContinue){
        this.requiresContinue = requiresContinue;
    }
    public void start(){
        for(CinematicScene s: scenes){
            s.reset();
        }
        isStarted = true;
        index = 0;
        isComplete = false;

    }

    public void stop(){
        //Gdx.app.log("CINEMATIC", "Cinematic is complete.");
        isComplete = true;
        adjustCamera(1f);
        this.setPosition(initialPosition.x, initialPosition.y);
    }

    public boolean isComplete(){
        return isComplete;
    }

    public boolean isContinueRequired(){
        return requiresContinue;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(isStarted && !isComplete){
            if(scenes.size > index){

                CinematicScene scene = scenes.get(index);
                scene.update(delta);
                scene = scenes.get(index);
                //Reset Zoom Level
                adjustCamera(scene.currentZoom);

                //Reset Camera Position
                this.setPosition(scene.currentPosition.x, scene.currentPosition.y);

                textureRegion = animation.getKeyFrames()[index];

                if(scene.isComplete()){
                    index++;
                    if(index >= scenes.size){
                        if(shouldLoop){
                            index = 0;
                        }else if(!requiresContinue){
                            stop();
                        }
                    }
                }
            }else if(!requiresContinue){
                stop();
            }
        }
    }

    @Override
    protected void drawBefore(Batch batch, float parentAlpha) {
        if(this.clearColor != null){
            Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }
        super.drawBefore(batch, parentAlpha);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    protected void drawFull(Batch batch, float parentAlpha) {
        super.drawFull(batch, parentAlpha);
    }

    private void adjustCamera(float zoomLevel){
        //Reset Camera Zoom
        Camera cam = getStage().getCamera();
        if(cam instanceof OrthographicCamera){
            ((OrthographicCamera)cam).zoom = zoomLevel;
        }
    }


}
