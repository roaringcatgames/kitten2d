package com.kasetagen.engine.gdx.scenes.scene2d.decorators;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericGroup;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 2/9/15
 * Time: 9:53 PM
 */
public class OffsetColliderDecorator implements ActorDecorator {

    private Vector2 offsets;
    private Vector2 dimensions;

    public OffsetColliderDecorator(Vector2 offsetValues, Vector2 dimensionValues){
        this.offsets = offsetValues;
        this.dimensions = dimensionValues;
    }

    public OffsetColliderDecorator(float xOffset, float yOffset, float xDimension, float yDimension){
        this.offsets = new Vector2(xOffset, yOffset);
        this.dimensions = new Vector2(xDimension, yDimension);
    }

    @Override
    public void applyAdjustment(Actor a, float delta) {
        Vector2 coords = a.localToStageCoordinates(new Vector2(offsets.x, offsets.y));
        if(a instanceof GenericActor){
            ((GenericActor) a).collider.set(coords.x, coords.y, dimensions.x, dimensions.y);//a.getX() + offsets.x, a.getY() + offsets.y, dimensions.x, dimensions.y);
        }else if(a instanceof GenericGroup){
            ((GenericGroup) a).collider.set(coords.x, coords.y, dimensions.x, dimensions.y);//a.getX() + offsets.x, a.getY() + offsets.y, dimensions.x, dimensions.y);
        }
    }
}
