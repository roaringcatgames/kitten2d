package com.kasetagen.engine.gdx.scenes.scene2d.decorators;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;
import com.kasetagen.engine.gdx.scenes.scene2d.IActorSpawner;
import com.kasetagen.engine.gdx.scenes.scene2d.actors.GenericActor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/25/15
 * Time: 1:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class ParticleSpawnerDecorator implements ActorDecorator {

    private IActorSpawner spawner;

    private float frequency;
    private float xRange;
    private float yRange;

    private float elapsedTime = 0f;

    private Vector2 leftBounds;
    private Vector2 rightBounds;

    private boolean useTargetY = false;
    private float targetY = 0f;
    private boolean useTargetX = false;
    private float targetX = 0f;


    public ParticleSpawnerDecorator(float frequency, float xRange, float yRange,
                                    Vector2 leftBound, Vector2 rightBound,
                                    IActorSpawner spawner){
        this.frequency = frequency;
        this.xRange = xRange;
        this.yRange = yRange;
        this.spawner = spawner;

        this.leftBounds = leftBound;
        this.rightBounds = rightBound;
    }

    public void setFrequency(float freq){
        this.frequency = freq;
    }

    public void setTargetX(float tgt, boolean use){
        targetX = tgt;
        useTargetX = use;
    }

    public void setTargetY(float tgt, boolean use){
        targetY = tgt;
        useTargetY = use;
    }

    @Override
    public void applyAdjustment(Actor a, float delta) {

        elapsedTime += delta;
        if(elapsedTime >= frequency){
            elapsedTime = 0f;
            GenericActor particle = spawner.spawn();
            if(particle != null){
                float aCenterX = a.getX()+a.getOriginX();
                float aCenterY = a.getY() + a.getOriginY();
                particle.setPosition(aCenterX, aCenterY);

                float tgtX = useTargetX ? targetX : aCenterX+xRange;
                float tgtY = useTargetY ? targetY : aCenterY+yRange;

                if(tgtX < leftBounds.x){
                    tgtX = leftBounds.x;
                }else if(tgtX >= (rightBounds.x-(particle.getWidth()/2))){
                    tgtX = rightBounds.x-particle.getWidth();
                }

                if(tgtY < leftBounds.y){
                    tgtY = leftBounds.y;
                }else if(tgtY >= (rightBounds.y-(particle.getHeight()/2))){
                    tgtY = rightBounds.y-particle.getHeight();
                }

                particle.addAction(Actions.moveTo(tgtX, tgtY, frequency));
                if(particle.getStage() == null){
                    a.getParent().addActor(particle);
                }

                xRange *= -1;
                yRange *= -1;
            }
        }
    }
}
