package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 11/23/14
 * Time: 10:38 PM
 */
public class CinematicScene {

    public float duration;
    public Vector2 startPosition;
    public Vector2 currentPosition;
    public Vector2 endPosition;
    public float startZoom = 0f;
    public float endZoom = 0f;
    public float currentZoom = 1f;
    //public String maskName;
    public TextureRegion mask;
    //public String  musicName;
    public Sound music;
    public float musicVolume = 1f;
    public int index;

    private float elapsedTime = 0f;
    private Vector2 speed;
    private float cameraSpeed = 0f;


    public CinematicScene(float duration,
                          float startX, float startY,
                          float endX, float endY,
                          float startZoom, float endZoom){
        this.duration = duration;
        this.startPosition = new Vector2(startX, startY);
        this.endPosition = new Vector2(endX, endY);
        this.currentPosition = new Vector2(startPosition);
        this.startZoom = startZoom;
        this.endZoom = endZoom;

        speed = new Vector2((endPosition.x - startPosition.x)/duration, (endPosition.y-startPosition.y)/duration);
        cameraSpeed = endZoom == startZoom ? 0f : (endZoom-startZoom)/duration;
    }

    public void update(float delta){
        if(!isStarted()){
            if(music != null){
                music.play(musicVolume);
            }
        }
        elapsedTime += delta;
        currentPosition.x += speed.x * delta;
        currentPosition.y += speed.y * delta;
        currentZoom += cameraSpeed * delta;

        if(isComplete()){
            if(music != null){
                music.stop();
            }
        }
    }

    public boolean isStarted(){
        return elapsedTime > 0f;
    }

    public boolean isComplete(){
        return elapsedTime >= duration;
    }

    public void reset(){
        elapsedTime = 0f;
        currentPosition.x = startPosition.x;
        currentPosition.y = startPosition.y;
        currentZoom = startZoom;
    }
}
