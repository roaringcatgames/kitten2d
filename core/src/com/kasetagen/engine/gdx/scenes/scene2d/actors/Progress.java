package com.kasetagen.engine.gdx.scenes.scene2d.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 3/23/15
 * Time: 8:19 PM
 */
public class Progress extends GenericActor{

    private float progress = 0f;
    public Progress(float x, float y, float width, float height, TextureRegion textureRegion, Color color) {
        super(x, y, width, height, textureRegion, color);
    }

    public void setProgress(float value){
        progress = value;
    }

    public float getProgress(){
        return progress;
    }
    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.end();
        batch.begin();
        //Set the projection matrix, and line shape
        debugRenderer.setProjectionMatrix(getStage().getCamera().combined);
        debugRenderer.begin(ShapeRenderer.ShapeType.Filled);
        //Draw the red origin marker
        debugRenderer.setColor(getColor());

        float progressWidth = getStage().getWidth() * progress;
        debugRenderer.rect(0f, 0f, progressWidth, getHeight());
        //End our shapeRenderer, flush the batch, and re-open it for future use as it was open
        // coming in.
        debugRenderer.end();
        batch.end();
        batch.begin();
    }
}
