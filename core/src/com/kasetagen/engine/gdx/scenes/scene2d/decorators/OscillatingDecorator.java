package com.kasetagen.engine.gdx.scenes.scene2d.decorators;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kasetagen.engine.gdx.scenes.scene2d.ActorDecorator;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/26/14
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class OscillatingDecorator implements ActorDecorator {

    private float minRotation = -5f;
    private float maxRotation = 5f;
    private float rotationSpeed = 1f;

    private int direction = 1;

    /**
     * This will continuously rotate the actor between the minimum and maximum rotation at the given rotation speed.
     * @param minRotation - Minmum rotation value before switching rotation directions.
     * @param maxRotation - Maximum rotation value before switching directions.
     * @param rotationSpeed - The rotation speed in rotation units/second. (Will be multiplied by frame delta to determine per-frame rotation adjustment)
     */
    public OscillatingDecorator(float minRotation, float maxRotation, float rotationSpeed){
        this.minRotation = minRotation;
        this.maxRotation = maxRotation;
        this.rotationSpeed = rotationSpeed;
    }

    public OscillatingDecorator(float minRotation) {
        this.minRotation = minRotation;
    }

    public void setRotationSpeed(float newSpeed){
        this.rotationSpeed = newSpeed;
    }

    @Override
    public void applyAdjustment(Actor a, float delta) {

        boolean isAboveMax = a.getRotation() >= maxRotation;
        boolean isBelowMin = a.getRotation() <= minRotation;

        if(isAboveMax || isBelowMin){
            if(isAboveMax){
                a.setRotation(maxRotation);
            }else{
                a.setRotation(minRotation);
            }
            direction *= -1;
        }

        a.setOrigin(a.getWidth()/2, a.getHeight());
        float adjustment = rotationSpeed * direction * delta;
        a.setRotation(a.getRotation()+adjustment);
    }
}

