package com.kasetagen.engine.gdx.scenes.scene2d;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 1/24/15
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
public enum ActorDirection {
    LEFT, RIGHT, UP, DOWN
}
