package com.kasetagen.engine.gdx.scenes.scene2d;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 10/26/14
 * Time: 2:29 PM
 */
public interface ActorDecorator {
    public void applyAdjustment(Actor a, float delta);
}
