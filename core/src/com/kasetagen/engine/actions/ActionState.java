package com.kasetagen.engine.actions;

//Trying to make GWT work
public class ActionState {//{  implements Cloneable{
	
	private ActionStatus actionStatus = ActionStatus.OFF;
	private long timeStamp;
	
	public ActionStatus getActionStatus() {
		return actionStatus;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}

	public ActionState(){}
	
	public ActionState(ActionStatus status){
		updateOnKeyEvent(status);
	}
	
	public void updateOnGameEvent(ActionStatus status){
		switch (status) {
			case FIRING:
					actionStatus = ActionStatus.OFF_TO_FIRING;
					break;
			case OFF:
					actionStatus = ActionStatus.FIRING_TO_OFF;
					break;
			default:
				break;
		}
		
		timeStamp = System.currentTimeMillis();	
		
		//System.out.println(actionStatus);
	}
	
	public void updateOnKeyEvent(ActionStatus status){
		switch (status) {
			case FIRING:
					actionStatus = ActionStatus.OFF_TO_FIRING;
					break;
			case OFF:
					actionStatus = ActionStatus.FIRING_TO_OFF;
					break;
			default:
				break;
		}

		timeStamp = System.currentTimeMillis();	
		
		//System.out.println(actionStatus);
	}
	
	public void updateAtRender(){
		switch (actionStatus) {
			case OFF_TO_FIRING:
					actionStatus = ActionStatus.FIRING;
					break;
			case FIRING_TO_OFF:
					actionStatus = ActionStatus.OFF;
					break;
			default:
				break;
		}
				
		timeStamp = System.currentTimeMillis();
		
		//System.out.println(actionStatus);
	}

    //TRYING TO MAKE GWT work
//	public Object clone() throws CloneNotSupportedException{
//		return (ActionState)super.clone();
//	}
	
	public String toString(){
		return ""+actionStatus;
	}
}
