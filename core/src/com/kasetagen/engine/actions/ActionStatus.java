package com.kasetagen.engine.actions;

public enum ActionStatus {
	OFF,
	OFF_TO_FIRING,
	FIRING,
	FIRING_TO_OFF
}
