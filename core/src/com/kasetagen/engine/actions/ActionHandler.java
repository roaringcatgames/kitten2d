package com.kasetagen.engine.actions;

import com.kasetagen.engine.config.DefaultKeyToActionMap;

import java.util.HashMap;

public class ActionHandler {
	// Key handler
	protected HashMap<ActionType, ActionState> actionMap = new HashMap<ActionType, ActionState>();	
	protected HashMap<Integer, ActionType> keyToActionsMap;
	private HashMap<ActionType, ActionState> actionMapClone = new HashMap<ActionType, ActionState>();
	
	public ActionHandler(){
		keyToActionsMap = new DefaultKeyToActionMap();
	}
	
	private void updateActionMapAndClone(){
		actionMapClone.clear();
		for(ActionType key:actionMap.keySet()){
			try {
                //Trying to get GWT to work
//				actionMapClone.put(key, (ActionState)actionMap.get(key).clone());
                actionMapClone.put(key, actionMap.get(key));
				ActionState actionState = actionMap.get(key);
				//System.out.println(key+" "+actionState.getActionStatus()+" "+actionMap.size());
				actionState.updateAtRender();
				
//				if(actionState.getActionStatus().equals(ActionStatus.OFF)){
//					System.out.print("remove "+key+" "+actionState.getActionStatus()+" ");
//					ActionState tempState = actionMap.remove(key);
//					System.out.println(tempState+" "+actionMap.size());
//				}
				
				//player.update(actionMapClone);
            //Trying to get GWT to work
			//} catch (CloneNotSupportedException e) {
            }catch(Exception e){
				e.printStackTrace();
				System.exit(1); //This should never be possible
			}
		}
	}
}
