package com.kasetagen.engine;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 1:56 AM
 */
public interface IGameProcessor {

    public String getStartScreen();
    public boolean isLoaded();
    public float getLoadingProgress();
    public AssetManager getAssetManager();
    public void changeToScreen(String screenName);
    public void setBGMusic(Music music);
    public void setBGMusicVolume(float newVolume);
    public String getStoredString(String key);
    public String getStoredString(String key, String defaultValue);
    public int getStoredInt(String key);
    public float getStoredFloat(String key);

    public void saveGameData(IDataSaver saver);
}
