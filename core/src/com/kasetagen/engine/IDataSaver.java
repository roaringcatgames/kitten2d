package com.kasetagen.engine;

import com.badlogic.gdx.Preferences;

/**
 * Created with IntelliJ IDEA.
 * User: barry
 * Date: 12/6/14
 * Time: 1:57 AM
 * To change this template use File | Settings | File Templates.
 */
public interface IDataSaver {
    public void updatePreferences(Preferences prefs);
}
