package com.kasetagen.engine.config;

import com.badlogic.gdx.Input.Keys;
import com.kasetagen.engine.actions.ActionType;

import java.util.HashMap;

public class DefaultKeyToActionMap extends HashMap<Integer, ActionType>{

	private static final long serialVersionUID = -8939600938378490319L;

	public DefaultKeyToActionMap(){
		this.put(Keys.ESCAPE, ActionType.ACTION_ESCAPE);
		this.put(Keys.ENTER, ActionType.ACTION_ENTER);
		
		this.put(Keys.A, ActionType.LEFT);
		this.put(Keys.D, ActionType.RIGHT);
		this.put(Keys.W, ActionType.UP);
		this.put(Keys.S, ActionType.DOWN);
		this.put(Keys.P, ActionType.PAUSE);
		this.put(Keys.SPACE, ActionType.JUMP);
		
		this.put(Keys.LEFT, 	ActionType.LEFT);
		this.put(Keys.RIGHT, 	ActionType.RIGHT);
		this.put(Keys.UP, 		ActionType.UP);
		this.put(Keys.DOWN, 	ActionType.DOWN);
				
		this.put(Keys.TAB, 		ActionType.FUNCTION_1);
		this.put(Keys.END, 		ActionType.CONSOLE);
		this.put(Keys.F1, 		ActionType.CONSOLE);
		this.put(Keys.DEL,		ActionType.CONSOLE);
		this.put(Keys.F2, 		ActionType.FUNCTION_2);
		this.put(Keys.F3, 		ActionType.FUNCTION_3);
		this.put(Keys.F4, 		ActionType.FUNCTION_4);
		this.put(Keys.F5, 		ActionType.FUNCTION_5);
		this.put(Keys.F6, 		ActionType.FUNCTION_6);
		this.put(Keys.F7, 		ActionType.FUNCTION_7);
		this.put(Keys.F8, 		ActionType.FUNCTION_8);
		//this.put(Keys., ActionType.CONSOLE);
	}
}